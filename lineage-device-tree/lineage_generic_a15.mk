#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from generic_a15 device
$(call inherit-product, device/unknown/generic_a15/device.mk)

PRODUCT_DEVICE := generic_a15
PRODUCT_NAME := lineage_generic_a15
PRODUCT_BRAND := Huawei
PRODUCT_MODEL := generic_a15
PRODUCT_MANUFACTURER := unknown

PRODUCT_GMS_CLIENTID_BASE := android-unknown

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="generic_a15-user 12 SP1A.210812.016 eng.huawei.20240725.172831 dev-keys"

BUILD_FINGERPRINT := Huawei/generic_a15/generic_a15:12/SP1A.210812.016/huawei07251736:user/dev-keys
